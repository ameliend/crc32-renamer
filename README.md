[![coverage](https://gitlab.com/ameliend/crc32-renamer/badges/main/coverage.svg)](https://gitlab.com/ameliend/crc32-renamer/-/commits/main)
[![vscode-editor](https://badgen.net/badge/icon/visualstudio?icon=visualstudio&label)](https://code.visualstudio.com/)
[![uv](https://img.shields.io/endpoint?url=https://raw.githubusercontent.com/astral-sh/uv/main/assets/badge/v0.json)](https://github.com/astral-sh/uv)
[![Ruff](https://img.shields.io/endpoint?url=https://raw.githubusercontent.com/astral-sh/ruff/main/assets/badge/v2.json)](https://github.com/astral-sh/ruff)
[![Copier](https://img.shields.io/endpoint?url=https://raw.githubusercontent.com/copier-org/copier/master/img/badge/badge-grayscale-inverted-border-orange.json)](https://github.com/copier-org/copier)
[![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)
[![API Documentation](https://badgen.net/badge/icon/API%20documentation?icon=gitlab&label&color=cyan)](https://ameliend.gitlab.io/crc32-renamer)

# Crc32 Renamer

<p align="center">
  <img src="./resources/logo.png">
</p>

This python script will rename a list a provided files by their crc32 checksums using sha256 hash,
it will likely helps you also to remove dupplicates files discovered by
their same generated hash.

## 📥 Installation

1. Download the source code of the Sorter by clicking **Code** > **zip**, and unzip it somewhere on your computer.

<p align="center">
  <img src="./resources/download.png">
</p>

2. Navigate to where you unzipped Sorter's source code using the command `cd`, for example:

    ```
    cd /d D:\crc32_renamer
    ```

3. Install the crc32-renamer

    ```
    pip install .
    ```

You should see the a shortcut **"hash"** appear in the **Send to** menu.

I just have to select one or several files and then click on "Send to".
It also work with folders, it will check all sub-folder files.

<p align="center">
  <img src="./resources/Screenshot_2.png">
</p>
