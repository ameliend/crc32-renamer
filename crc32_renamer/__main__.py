"""Entry point for Crc32 Renamer."""

from crc32_renamer.cli import main_cli

if __name__ == "__main__":
    main_cli()
