"""Top-level package for Crc32 Renamer."""

from crc32_renamer._version import __version__
from crc32_renamer.hash import search_and_rename

__author__ = """Amelien Deshams"""
__email__ = "a.deshams+git@slmail.me"
__all__ = [
    "__version__",
    "search_and_rename",
]
