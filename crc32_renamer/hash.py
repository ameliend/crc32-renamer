from __future__ import annotations

from binascii import crc32
from hashlib import sha256
from pathlib import Path
from sys import argv

from logger import Logger

logger = Logger("Hash")


def generate_crc32_filename(filepath: Path) -> str:
    """Generate a unique identifier for a file from its content.

    Parameters
    ----------
    filepath : Path
        The path of the file in which you want to generate the identifier.

    Returns
    -------
    str
        The CRC32 checksum of the sha256 hash of the file contents, followed by the file extension.
    """
    _hexdigest = sha256(filepath.read_bytes()).hexdigest()
    return f"{crc32(_hexdigest.encode())!s}{filepath.suffix}"


def rename_file_by_crc32_checksum(filepath: Path) -> None:
    """Rename a file using the CRC32 checksum of its contents as identifier.

    Parameters
    ----------
    filepath : Path
        The path to the file that you want to rename.
    """
    crc32_identifier = generate_crc32_filename(filepath)
    if filepath.name != crc32_identifier:
        renamed_filepath = filepath.with_name(crc32_identifier)
        try:
            filepath.rename(renamed_filepath)
            logger.success(f"File renamed to CRC32 : '{filepath}' to '{renamed_filepath}'")
        except FileExistsError:
            filepath.unlink()
            logger.warning("Dupplicate file '%s'", filepath)
    else:
        logger.info("Hash ok '%s'", filepath)


def search_and_rename(filepaths: list[str]) -> None:
    """Search for files in a list of paths and rename them using the CRC32 checksum as identifier.

    Parameters
    ----------
    filepaths : List[str]
        The list of paths where you want to search for and rename files.
    """
    for item in filepaths:
        filepath_item = Path(item)
        if filepath_item.is_dir():
            for child_item in filepath_item.iterdir():
                if child_item.is_file():
                    rename_file_by_crc32_checksum(child_item)
        else:
            rename_file_by_crc32_checksum(filepath_item)


if __name__ == "__main__":
    logger.debug(argv[1:])
    search_and_rename(filepaths=argv[1:])
