"""Console script for CRC32 renamer."""

from sys import argv

from logger import Logger

from crc32_renamer import search_and_rename

logger = Logger("Hash")


def main_cli() -> None:
    """Console script for CRC32 renamer."""
    logger.debug(argv[1:])
    search_and_rename(filepaths=argv[1:])
