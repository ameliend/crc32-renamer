import pytest


@pytest.fixture
def sample_file(tmp_path):
    filepath = tmp_path / "sample.txt"
    filepath.write_text("Sample Content")
    return filepath
