from crc32_renamer.hash import generate_crc32_filename


def test_generate_crc32_checksum(sample_file):
    expected_filename = "3173353327.txt"
    generated_filename = generate_crc32_filename(sample_file)
    assert generated_filename == expected_filename
