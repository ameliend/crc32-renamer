## [1.0.1](https://gitlab.com/ameliend/crc32-renamer/compare/v1.0.0...v1.0.1) (2024-11-01)

### 🦊 CI/CD

* **setup:** fix encoding issue ([492123c](https://gitlab.com/ameliend/crc32-renamer/commit/492123c1e40a1a98c3fc662fab1386cde76f3b5c))

## [1.0.0](https://gitlab.com/ameliend/crc32-renamer/compare/...v1.0.0) (2024-11-01)

### 📔 Docs

* update docs ([b151d34](https://gitlab.com/ameliend/crc32-renamer/commit/b151d347107e7d603b0f9cf69cdf938ec5bbb6f7))

### 🦊 CI/CD

* **release:** update path ([77230fb](https://gitlab.com/ameliend/crc32-renamer/commit/77230fb0b007588c64b1e1a40132d8811b4ffd8d))
* **ruff:** add ruff config ([4c9eade](https://gitlab.com/ameliend/crc32-renamer/commit/4c9eade18d8b7435dd77cc042d49e758c41d652b))
* **ruff:** top-level linter settings are deprecated in favour of their counterparts in the `lint` section ([a026238](https://gitlab.com/ameliend/crc32-renamer/commit/a026238889d3508d611a0ceb23d45d6f8a500eaa))
* **setup:** revert pyproject.toml setup workflow to setup.py ([75d8915](https://gitlab.com/ameliend/crc32-renamer/commit/75d891593f34a4412e2d071e8fc0711dcce646ee))

### 🧪 Tests

* add tests ([50293d8](https://gitlab.com/ameliend/crc32-renamer/commit/50293d82f0a87bea33c459a16e40970fa15b7b8f))

### 🚀 Features

* initial commit ([9ff55db](https://gitlab.com/ameliend/crc32-renamer/commit/9ff55dbd27a64239b4716e6dd662340c92e86af4))
